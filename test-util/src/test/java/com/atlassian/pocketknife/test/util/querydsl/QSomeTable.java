package com.atlassian.pocketknife.test.util.querydsl;

import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;

import java.sql.Types;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

public class QSomeTable extends EnhancedRelationalPathBase<QSomeTable>
{
    public final NumberPath<Long> id = createLongCol("ID").withIndex(1).ofType(Types.BIGINT).withSize(19).notNull().build();
    public final StringPath val = createStringCol("VAL").withIndex(2).ofType(Types.VARCHAR).withSize(2147483647).build();

    public QSomeTable() {
        super(QSomeTable.class, "AO_012345_SOME_TABLE");
    }
}
