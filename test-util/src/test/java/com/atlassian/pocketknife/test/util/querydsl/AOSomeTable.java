package com.atlassian.pocketknife.test.util.querydsl;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("SOME_TABLE")
public interface AOSomeTable extends Entity {

    String getVal();
    void setVal(String val);
}
