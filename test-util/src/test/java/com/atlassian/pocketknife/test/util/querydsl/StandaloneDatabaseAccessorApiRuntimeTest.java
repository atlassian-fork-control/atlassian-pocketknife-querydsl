package com.atlassian.pocketknife.test.util.querydsl;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class StandaloneDatabaseAccessorApiRuntimeTest {

    @Test
    public void testApiRuntimeMakesSense() throws Exception {
        StandaloneDatabaseAccessor standaloneDatabaseAccessor = new StandaloneDatabaseAccessor();

        StandaloneApiRuntime apiRuntime = standaloneDatabaseAccessor.getApiRuntime();
        assertThat(apiRuntime, notNullValue());

        assertThat(apiRuntime.getDatabaseAccessor(), equalTo(standaloneDatabaseAccessor));
        assertThat(apiRuntime.getOptionalAwareDatabaseAccessor(), equalTo(standaloneDatabaseAccessor));
        assertThat(apiRuntime.getEitherAwareDatabaseAccessor(), equalTo(standaloneDatabaseAccessor));

        assertThat(apiRuntime.getConfigurationEnrichment(), notNullValue());
        assertThat(apiRuntime.getDatabaseConnectionConverter(), notNullValue());
        assertThat(apiRuntime.getDialectProvider(), notNullValue());
        assertThat(apiRuntime.getSchemaStateProvider(), notNullValue());
        assertThat(apiRuntime.getStreamingQueryFactory(), notNullValue());
    }
}
