/*
 * Copyright 2011, Mysema Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.pocketknife.internal.querydsl.tables;

import com.atlassian.pocketknife.internal.querydsl.tables.ddl.CreateTableClause;
import com.atlassian.pocketknife.internal.querydsl.tables.ddl.DropTableClause;
import com.google.common.collect.Maps;
import com.querydsl.sql.H2Templates;
import com.querydsl.sql.HSQLDBTemplates;
import com.querydsl.sql.SQLTemplates;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Types;
import java.util.Map;

/**
 * Taken from QueryDSL tests themselves
 */
@SuppressWarnings({"SqlNoDataSourceInspection", "SqlDialectInspection"})
public final class TestConnections {

    private static final int TEST_ROW_COUNT = 100;
    // datetest
    private static final String CREATE_TABLE_DATETEST = "create table DATE_TEST(DATE_TEST date)";
    // survey
    private static final String CREATE_TABLE_SURVEY =
            "create table SURVEY(ID int auto_increment, NAME varchar(30), NAME2 varchar(30))";
    // test
    private static final String CREATE_TABLE_TEST = "create table TEST(NAME varchar(255))";
    // timetest
    private static final String CREATE_TABLE_TIMETEST = "create table TIME_TEST(TIME_TEST time)";
    // typetest
    private static final String CREATE_TABLE_TYPETEST =
            "create table TYPE_TEST(T1 varchar(30), T2 varchar(30))";

    private static final String INSERT_INTO_TEST_VALUES = "insert into TEST values(?)";
    private static ThreadLocal<Connection> connHolder = new ThreadLocal<>();
    private static ThreadLocal<Target> targetHolder = new ThreadLocal<>();
    private static ThreadLocal<Statement> stmtHolder = new ThreadLocal<>();

    private static boolean h2Inited, hsqlInited;

    private TestConnections() {
    }

    public static void close() throws SQLException {
        if (stmtHolder.get() != null) {
            stmtHolder.get().close();
        }
        if (connHolder.get() != null) {
            connHolder.get().close();
        }
    }

    public static Connection getConnection() {
        return connHolder.get();
    }

    private static Connection getHSQL() throws SQLException, ClassNotFoundException {
        Class.forName("org.hsqldb.jdbcDriver");
        String url = "jdbc:hsqldb:target/tutorial";
        return DriverManager.getConnection(url, "sa", "");
    }

    private static Connection getH2() throws SQLException, ClassNotFoundException {
        try {
            File tempDbFile = File.createTempFile("h2db","test");
            tempDbFile.deleteOnExit();
            Class.forName("org.h2.Driver");
            String url = "jdbc:h2:"+ tempDbFile.getPath() + ";LOCK_MODE=0";
            return DriverManager.getConnection(url, "sa", "");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static CreateTableClause createTable(SQLTemplates templates, String table) {
        return new CreateTableClause(connHolder.get(), templates, table);
    }

    public static void dropTable(SQLTemplates templates, String table) throws SQLException {
        new DropTableClause(connHolder.get(), templates, table).execute();
    }


    private static void createEmployeeTables(SQLTemplates templates) {
        createTable(templates, "EMPLOYEE")
                .column("ID", Integer.class).notNull()
                .column("FIRSTNAME", String.class).size(50)
                .column("LASTNAME", String.class).size(50)
                .column("SALARY", Double.class)
                .column("DATEFIELD", Date.class)
                .column("TIMEFIELD", Time.class)
                .column("SUPERIOR_ID", Integer.class)
                .primaryKey("PK_EMPLOYEE", "ID")
                .foreignKey("FK_SUPERIOR", "SUPERIOR_ID").references("EMPLOYEE", "ID")
                .execute();

        createTable(templates, "AO_EMPLOYEE")
                .column("ID", Integer.class).notNull()
                .column("FIRSTNAME", String.class).size(50)
                .column("LASTNAME", String.class).size(50)
                .column("SALARY", Double.class)
                .column("DATEFIELD", Date.class)
                .column("TIMEFIELD", Time.class)
                .column("SUPERIOR_ID", Integer.class)
                .primaryKey("PK_AO_EMPLOYEE", "ID")
                .foreignKey("FK_A_SUPERIOR", "SUPERIOR_ID").references("EMPLOYEE", "ID")
                .execute();
    }

    public static Map<Integer, String> getSpatialData() {
        Map<Integer, String> m = Maps.newHashMap();
        // point
        m.put(1, "POINT (2 2)");
        m.put(2, "POINT (8 7)");
        m.put(3, "POINT (1 9)");
        m.put(4, "POINT (9 2)");
        m.put(5, "POINT (4 4)");
        // linestring
        m.put(6, "LINESTRING (30 10, 10 30)");
        m.put(7, "LINESTRING (30 10, 10 30, 40 40)");
        // polygon
        m.put(8, "POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10), (20 30, 35 35, 30 20, 20 30))");
        m.put(9, "POLYGON ((35 10, 45 45, 15 40, 10 20, 35 10), (20 30, 35 35, 30 20, 20 30))");
        // multipoint
        m.put(11, "MULTIPOINT (10 40, 40 30)");
        m.put(11, "MULTIPOINT (10 40, 40 30, 20 20, 30 10)");
        // multilinestring
        m.put(12, "MULTILINESTRING ((10 10, 20 20, 10 40), (40 40, 30 30, 40 20, 30 10))");
        m.put(13, "MULTILINESTRING ((10 10, 20 20, 10 40))");
        // multipolygon
        m.put(14, "MULTIPOLYGON (((30 20, 45 40, 10 40, 30 20)), ((15 5, 40 10, 10 20, 5 10, 15 5)))");
        m.put(15, "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)), " +
                "((20 35, 10 30, 10 10, 30 5, 45 20, 20 35), " +
                "(30 20, 20 15, 20 25, 30 20)))");

        // XXX POLYHEDRALSURFACE not supported

        /* GEOMETRYCOLLECTION(POINT(4 6),LINESTRING(4 6,7 10))
           CIRCULARSTRING(1 5, 6 2, 7 3)
           COMPOUNDCURVE(CIRCULARSTRING(0 0,1 1,1 0),(1 0,0 1))
           CURVEPOLYGON(CIRCULARSTRING(-2 0,-1 -1,0 0,1 -1,2 0,0 2,-2 0),(-1 0,0 0.5,1 0,0 1,-1 0))
           MULTICURVE((5 5,3 5,3 3,0 3),CIRCULARSTRING(0 0,2 1,2 2))
           TRIANGLE((0 0 0,0 1 0,1 1 0,0 0 0))
           TIN (((0 0 0, 0 0 1, 0 1 0, 0 0 0)), ((0 0 0, 0 1 0, 1 1 0, 0 0 0)))
        */
        return m;
    }


    private static void insertTestValues(Connection c) throws SQLException {
        try (PreparedStatement pstmt = c.prepareStatement(INSERT_INTO_TEST_VALUES)) {
            for (int i = 0; i < TEST_ROW_COUNT; i++) {
                pstmt.setString(1, "name" + i);
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        }
    }

    @SuppressWarnings("unused")
    public static void initH2() throws SQLException, ClassNotFoundException {
        targetHolder.set(Target.H2);
        SQLTemplates templates = new H2Templates();
        Connection c = getH2();
        connHolder.set(c);
        Statement stmt = c.createStatement();
        stmtHolder.set(stmt);

        // qtest
        stmt.execute("drop table QTEST if exists");
        stmt.execute("create table QTEST (ID int IDENTITY(1,1) NOT NULL,  C1 int NULL)");

        // survey
        stmt.execute("drop table SURVEY if exists");
        stmt.execute(CREATE_TABLE_SURVEY);
        stmt.execute("insert into SURVEY values (1, 'Hello World', 'Hello');");
        stmt.execute("alter table SURVEY alter column id int auto_increment");

        // test
        stmt.execute("drop table TEST if exists");
        stmt.execute(CREATE_TABLE_TEST);
        insertTestValues(c);

        // employee
        stmt.execute("drop table EMPLOYEE if exists");
        stmt.execute("drop table AO_EMPLOYEE if exists");
        createEmployeeTables(templates);
        stmt.execute("alter table EMPLOYEE alter column id int auto_increment");
        stmt.execute("alter table AO_EMPLOYEE alter column id int auto_increment");
        addEmployees(INSERT_INTO_EMPLOYEE("EMPLOYEE"));
        addEmployees(INSERT_INTO_EMPLOYEE("AO_EMPLOYEE"));


        // date_test and time_test
        stmt.execute("drop table TIME_TEST if exists");
        stmt.execute("drop table DATE_TEST if exists");
        stmt.execute(CREATE_TABLE_TIMETEST);
        stmt.execute(CREATE_TABLE_DATETEST);
        stmt.execute(CREATE_TABLE_TYPETEST);
        h2Inited = true;
    }

    private static String INSERT_INTO_EMPLOYEE(String tableName) {
        return "insert into " + tableName + " " +
                "(ID, FIRSTNAME, LASTNAME, SALARY, DATEFIELD, TIMEFIELD, SUPERIOR_ID) " +
                "values (?,?,?,?,?,?,?)";

    }

    public static void initHSQL() throws SQLException, ClassNotFoundException {
        initHSQLImpl(true);
    }

    @SuppressWarnings("unused")
    public static void initHSQL23() throws SQLException, ClassNotFoundException {
        initHSQLImpl(false);
    }

    private static void initHSQLImpl(boolean old18HSQL) throws SQLException, ClassNotFoundException {
        targetHolder.set(Target.HSQLDB);
        SQLTemplates templates = new HSQLDBTemplates();
        Connection c = getHSQL();
        connHolder.set(c);
        Statement stmt = c.createStatement();
        stmtHolder.set(stmt);

        if (hsqlInited) {
            return;
        }

        // dual
        stmt.execute("drop table DUAL if exists");
        stmt.execute("create table DUAL ( DUMMY varchar(1) )");
        stmt.execute("insert into DUAL (DUMMY) values ('X')");

        // survey
        stmt.execute("drop table SURVEY if exists");
        //stmt.execute(CREATE_TABLE_SURVEY);
        stmt.execute("create table SURVEY(" +
                "ID int generated by default as identity, " +
                "NAME varchar(30)," +
                "NAME2 varchar(30))");
        stmt.execute("insert into SURVEY values (1, 'Hello World', 'Hello')");

        // test
        stmt.execute("drop table TEST if exists");
        stmt.execute(CREATE_TABLE_TEST);
        insertTestValues(c);

        // employee
        stmt.execute("drop table EMPLOYEE if exists");
        stmt.execute("drop table AO_EMPLOYEE if exists");

        // BB
        if (old18HSQL) {
            stmt.execute("create table EMPLOYEE(" +
                    "ID int generated by default as identity, " +
                    "FIRSTNAME varchar(50)," +
                    "LASTNAME varchar(50)," +
                    "SALARY DECIMAL, " +
                    "DATEFIELD DATE, " +
                    "TIMEFIELD TIME, " +
                    "SUPERIOR_ID INT" +
                    ") ");

            stmt.execute("create table AO_EMPLOYEE(" +
                    "ID int generated by default as identity, " +
                    "FIRSTNAME varchar(50)," +
                    "LASTNAME varchar(50)," +
                    "SALARY DECIMAL, " +
                    "DATEFIELD DATE, " +
                    "TIMEFIELD TIME, " +
                    "SUPERIOR_ID INT" +
                    ") ");
        } else {
            createEmployeeTables(templates);
            stmt.execute("alter table EMPLOYEE alter column id int generated by default as identity");
            stmt.execute("alter table AO_EMPLOYEE alter column id int generated by default as identity");
        }

        addEmployees(INSERT_INTO_EMPLOYEE("EMPLOYEE"));
        addEmployees(INSERT_INTO_EMPLOYEE("AO_EMPLOYEE"));

        // date_test and time_test
        stmt.execute("drop table TIME_TEST if exists");
        stmt.execute("drop table DATE_TEST if exists");
        stmt.execute("drop table TYPE_TEST if exists");
        stmt.execute(CREATE_TABLE_TIMETEST);
        stmt.execute(CREATE_TABLE_DATETEST);
        stmt.execute(CREATE_TABLE_TYPETEST);
        hsqlInited = true;
    }

    private static void addEmployee(String sql, int id, String firstName, String lastName,
                                    double salary, int superiorId) throws SQLException {
        PreparedStatement stmt = connHolder.get().prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.setString(2, firstName);
        stmt.setString(3, lastName);
        stmt.setDouble(4, salary);
        stmt.setDate(5, Constants.date);
        stmt.setTime(6, Constants.time);
        if (superiorId <= 0) {
            stmt.setNull(7, Types.INTEGER);
        } else {
            stmt.setInt(7, superiorId);
        }
        stmt.execute();
        stmt.close();
    }

    private static void addEmployees(String sql) throws SQLException {
        addEmployee(sql, 1, "Mike", "Smith", 160000, -1);
        addEmployee(sql, 2, "Mary", "Smith", 140000, -1);

        // Employee under Mike
        addEmployee(sql, 10, "Joe", "Divis", 50000, 1);
        addEmployee(sql, 11, "Peter", "Mason", 45000, 1);
        addEmployee(sql, 12, "Steve", "Johnson", 40000, 1);
        addEmployee(sql, 13, "Jim", "Hood", 35000, 1);

        // Employee under Mary
        addEmployee(sql, 20, "Jennifer", "Divis", 60000, 2);
        addEmployee(sql, 21, "Helen", "Mason", 50000, 2);
        addEmployee(sql, 22, "Daisy", "Johnson", 40000, 2);
        addEmployee(sql, 23, "Barbara", "Hood", 30000, 2);
    }

}