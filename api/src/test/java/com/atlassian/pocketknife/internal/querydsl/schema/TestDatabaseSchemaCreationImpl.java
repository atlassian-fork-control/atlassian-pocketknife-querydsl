package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearer;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearerImpl;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDatabaseSchemaCreationImpl {
    private static final String AO_SERVICE_NAME = "com.atlassian.activeobjects.external.ActiveObjects";

    private ServiceReference serviceReference;
    private BundleContext bundleContext;
    private DatabaseSchemaCreationImpl activeObjectsSchema;
    private FakeAo fakeAo;
    private PKQCacheClearer cacheClearer;

    private class FakeAo {
        private int count;

        @SuppressWarnings("UnusedDeclaration")
        public void flushAll() {
            count++;
        }
    }

    @Before
    public void setUp() throws Exception {
        cacheClearer = new PKQCacheClearerImpl(null);
        bundleContext = mock(BundleContext.class);
        serviceReference = mock(ServiceReference.class);
        activeObjectsSchema = new DatabaseSchemaCreationImpl(bundleContext, cacheClearer);
        activeObjectsSchema.postConstruction();
        fakeAo = new FakeAo();
    }

    @Test
    public void test_called_once_and_once_only() throws Exception {
        // assemble
        when(bundleContext.getServiceReference(AO_SERVICE_NAME)).thenReturn(serviceReference);
        when(bundleContext.getService(serviceReference)).thenReturn(fakeAo);

        // act
        activeObjectsSchema.prime();
        activeObjectsSchema.prime();
        activeObjectsSchema.prime();

        //assert
        assertThat(fakeAo.count, equalTo(1));
    }

    @Test
    public void test_when_no_service_ref() throws Exception {
        // assemble
        when(bundleContext.getServiceReference(AO_SERVICE_NAME)).thenReturn(null);

        // act
        activeObjectsSchema.prime();

        //assert
        assertThat(fakeAo.count, equalTo(0));
    }

    @Test
    public void test_when_no_service() throws Exception {
        // assemble
        when(bundleContext.getServiceReference(AO_SERVICE_NAME)).thenReturn(serviceReference);
        when(bundleContext.getService(serviceReference)).thenReturn(null);

        // act
        activeObjectsSchema.prime();

        //assert
        assertThat(fakeAo.count, equalTo(0));
    }

    @Test
    public void test_that_cached_gets_cleared_and_ao_side_effects_happens() throws Exception {

        when(bundleContext.getServiceReference(AO_SERVICE_NAME)).thenReturn(serviceReference);
        when(bundleContext.getService(serviceReference)).thenReturn(fakeAo);

        // act
        activeObjectsSchema.prime();

        //assert
        assertThat(fakeAo.count, equalTo(1));

        cacheClearer.clearAllCaches();

        // act
        activeObjectsSchema.prime();

        //assert
        assertThat(fakeAo.count, equalTo(2)); // it go un-cached!
    }
}