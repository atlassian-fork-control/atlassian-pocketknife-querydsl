package com.atlassian.pocketknife.internal.querydsl.cache;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class PKQCacheClearerImplTest {

    private PKQCacheClearerImpl classUnderTest;
    private boolean isJiraClearCache = true;

    @Before
    public void setUp() throws Exception {
        classUnderTest = new PKQCacheClearerImpl(null) {
            @Override
            boolean isClearCacheEvent(Object event) {
                return isJiraClearCache;
            }
        };
    }

    @Test
    public void test_cache_clearing_is_off_by_default() throws Exception {
        RunRecorder run = new RunRecorder();
        classUnderTest.registerCacheClearing(run);

        classUnderTest.onClearCache("clearcache");

        assertThat(run.ran(), equalTo(false));
    }

    @Test
    public void test_cache_clearing_is_on_by_system_property() throws Exception {
        System.setProperty("pkqdsl.react.to.clear.cache", "true");

        RunRecorder run = new RunRecorder();
        classUnderTest.registerCacheClearing(run);

        classUnderTest.onClearCache("clearcache");

        assertThat(run.ran(), equalTo(true));
    }

    @Test
    public void test_cache_clearing_is_off_by_system_property() throws Exception {
        System.setProperty("pkqdsl.react.to.clear.cache", "false");
        RunRecorder run = new RunRecorder();
        classUnderTest.registerCacheClearing(run);
        classUnderTest.onClearCache("clearcache");

        assertThat(run.ran(), equalTo(false));
    }


    @Test
    public void test_it_ignores_other_events() throws Exception {

        System.setProperty("pkqdsl.react.to.clear.cache", "true");

        RunRecorder run = new RunRecorder();
        isJiraClearCache = false;
        classUnderTest.registerCacheClearing(run);
        classUnderTest.onClearCache("clearcache");

        assertThat(run.ran(), equalTo(false));

    }

    static class RunRecorder implements Runnable {
        int runCount = 0;

        @Override
        public void run() {
            runCount++;
        }

        boolean ran() {
            return runCount > 0;
        }
    }
}