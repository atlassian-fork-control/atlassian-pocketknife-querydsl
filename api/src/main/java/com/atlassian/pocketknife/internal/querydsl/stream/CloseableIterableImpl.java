package com.atlassian.pocketknife.internal.querydsl.stream;

import com.atlassian.annotations.Internal;
import com.atlassian.pocketknife.api.querydsl.stream.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.tuple.Tuples;
import com.atlassian.pocketknife.internal.querydsl.util.fp.Fp;
import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Tuple functions are implemented in terms of this class.  Its a generic implementation of a {@link
 * com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable}
 */
@Internal
public class CloseableIterableImpl<S,T> implements CloseableIterable<T>
{
    private final CloseableIteratorImpl<S, T> closeableIterator;
    private final ClosePromise closePromise;

    public CloseableIterableImpl(CloseableIterator<S> srcIterator, Function<S, T> mapper, final ClosePromise parentPromise)
    {
        this(srcIterator, mapper, parentPromise, Fp.alwaysTrue(), Fp.alwaysTrue());
    }

    public CloseableIterableImpl(CloseableIterator<S> srcIterator, Function<S, T> mapper, final ClosePromise parentPromise, Predicate<S> filterPredicate, Predicate<S> takeWhilePredicate)
    {
        IteratorInstructions<S, T> instructions = new IteratorInstructions<>(srcIterator, mapper, parentPromise, filterPredicate, takeWhilePredicate);

        this.closeableIterator = new CloseableIteratorImpl<>(instructions);
        this.closePromise = new ClosePromise(parentPromise, this::closeImpl);
    }

    public CloseableIterableImpl(CloseableIteratorImpl<S, T> closeableIterator)
    {
        this.closeableIterator = closeableIterator;
        this.closePromise = closeableIterator.closePromise;
    }

    @Override
    public CloseableIterator<T> iterator()
    {
        return closeableIterator;
    }

    @Override
    public CloseableIterable<T> take(final int n)
    {
        checkArgument(n >= 0, "take(n) argument must be >= 0");
        checkState(!this.closeableIterator.hasStarted(), "You cant take(n) from an iterable that has been read");
        ensureNotClosed();

        Predicate<T> nTaken = nTakenPredicate(n);
        return takeWhile(nTaken);
    }

    @Override
    public CloseableIterable<T> takeWhile(final Predicate<T> takeWhilePredicate)
    {
        checkNotNull(takeWhilePredicate);
        checkState(!this.closeableIterator.hasStarted(), "You cant takeWhile() from an iterable that has been read");
        ensureNotClosed();

        final CloseableIteratorImpl<S, T> src = this.closeableIterator;

        Predicate<S> composedTakeWhile = Fp.compose(takeWhilePredicate, src.mapper);

        final IteratorInstructions<S, T> instructions = new IteratorInstructions<>(src.instructions);
        instructions.takeWhilePredicate = composedTakeWhile;

        final CloseableIteratorImpl<S, T> newIterator = new CloseableIteratorImpl<>(instructions);
        return new CloseableIterableImpl<>(newIterator);
    }

    @Override
    public CloseableIterable<T> filter(Predicate<T> filterPredicate)
    {
        checkNotNull(filterPredicate);
        ensureNotClosed();

        final CloseableIteratorImpl<S, T> src = this.closeableIterator;

        Predicate<S> composedFilterPredicate = Fp.compose(filterPredicate, src.mapper);
        IteratorInstructions<S, T> instructions = new IteratorInstructions<>(src.instructions);
        instructions.filterPredicate = composedFilterPredicate;

        CloseableIteratorImpl<S, T> newIterator = new CloseableIteratorImpl<>(instructions);
        return new CloseableIterableImpl<>(newIterator);
    }

    /**
     * A predicate that counts how many things go past and returns false once enough have
     * @param n the max number inclusive
     * @param <T> a type
     * @return true if less than n have been taken
     */
    public static <T> Predicate<T> nTakenPredicate(final int n)
    {
        return new Predicate<T>()
        {
            int takenSoFar = 0;

            @Override
            public boolean test(final T t)
            {
                if (takenSoFar < n)
                {
                    takenSoFar++;
                    return true;
                }
                return false;
            }
        };
    }

    @Override
    public <D> CloseableIterable<D> map(final Function<T, D> mapper)
    {
        ensureNotClosed();

        // we need to map from the old iterable of S,T to a new one of T,D
        final CloseableIteratorImpl<S, T> src = this.closeableIterator;

        final Function<S, D> composedMapper = input -> Fp.compose(mapper, src.mapper).apply(input);
        IteratorInstructions<S, D> instructions = new IteratorInstructions<>(src.srcIterator, composedMapper, closePromise, src.filterPredicate, src.takeWhilePredicate);

        CloseableIteratorImpl<S, D> composedIterator = new CloseableIteratorImpl<>(instructions);
        return new CloseableIterableImpl<>(composedIterator);
    }

    @Override
    public <D> CloseableIterable<D> map(final Expression<D> expr)
    {
        ensureNotClosed();

        Function<Tuple, D> extractColumn = Tuples.column(expr);
        return map(t -> ensureTupleQuery(t, extractColumn));
    }

    private <D> D ensureTupleQuery(final T t, final Function<Tuple, D> extractColumn)
    {
        if (!(t instanceof Tuple))
        {
            throw new IllegalStateException("The underlying query must be SQLQuery<Tuple> to call this method");
        }
        Tuple tuple = (Tuple) t;
        return extractColumn.apply(tuple);
    }

    @Override
    public <D> D foldLeft(final D initial, BiFunction<D, T, D> combiningFunction)
    {
        ensureNotClosed();

        try
        {
            D accumulator = initial;
            while (closeableIterator.hasNext())
            {
                accumulator = combiningFunction.apply(accumulator, closeableIterator.next());
            }
            return accumulator;
        }
        finally
        {
            close();
        }
    }

    private void ensureNotClosed()
    {
        if (closePromise.isClosed())
        {
            throw new IllegalStateException("This CloseableIterable has already been closed");
        }
    }


    @Override
    public Optional<T> fetchFirst()
    {
        ensureNotClosed();
        try
        {
            CloseableIterator<T> iterator = iterator();
            if (iterator.hasNext())
            {
                return Optional.of(iterator.next());
            }
            return Optional.empty();
        }
        finally
        {
            close();
        }
    }

    @Override
    public void foreach(final Consumer<T> effect)
    {
        ensureNotClosed();
        try
        {
            for (final T t : this)
            {
                effect.accept(t);
            }
        }
        finally
        {
            close();
        }
    }

    @Override
    public void forEach(final Consumer<? super T> action)
    {
        foreach(action::accept);
    }

    @Override
    public Spliterator<T> spliterator()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void close()
    {
        closePromise.close();
    }

    private void closeImpl()
    {
        closeableIterator.close();
    }

    /**
     * A simpler holder class that has the innards of the Iterator
     */
    static class IteratorInstructions<S, T>
    {
        private CloseableIterator<S> srcIterator;
        private Function<S, T> mapper;
        private ClosePromise closePromise;
        private Predicate<S> filterPredicate;
        private Predicate<S> takeWhilePredicate;

        public IteratorInstructions(final CloseableIterator<S> srcIterator, final Function<S, T> mapper, final ClosePromise closePromise, final Predicate<S> filterPredicate, final Predicate<S> takeWhilePredicate)
        {
            this.srcIterator = srcIterator;
            this.mapper = mapper;
            this.closePromise = closePromise;
            this.filterPredicate = filterPredicate;
            this.takeWhilePredicate = takeWhilePredicate;
        }

        IteratorInstructions(IteratorInstructions<S, T> instructions)
        {
            this.srcIterator = instructions.srcIterator;
            this.mapper = instructions.mapper;
            this.closePromise = instructions.closePromise;
            this.takeWhilePredicate = instructions.takeWhilePredicate;
            this.filterPredicate = instructions.filterPredicate;
        }
    }

    /**
     * The underlying Iterator that maps from S to T
     *
     * @param <S> the S-ness of S eg the source
     * @param <T> the T-ness of T eg the target
     */
    static class CloseableIteratorImpl<S, T> implements CloseableIterator<T>
    {
        private final CloseableIterator<S> srcIterator;
        private final Function<S, T> mapper;
        private final ClosePromise closePromise;
        private final Predicate<S> filterPredicate;
        private final Predicate<S> takeWhilePredicate;
        private final IteratorInstructions<S, T> instructions;

        int returnedSoFar;
        S nextObject;
        boolean nextObjectAccessed = true;

        CloseableIteratorImpl(IteratorInstructions<S, T> instructions)
        {
            this.instructions = instructions;
            this.srcIterator = instructions.srcIterator;
            this.mapper = instructions.mapper;
            this.closePromise = new ClosePromise(instructions.closePromise, srcIterator::close);
            this.filterPredicate = instructions.filterPredicate;
            this.takeWhilePredicate = instructions.takeWhilePredicate;
            this.returnedSoFar = 0;
        }


        boolean hasStarted()
        {
            return returnedSoFar > 0;
        }

        @Override
        public boolean hasNext()
        {
            if (!nextObjectAccessed)
            {
                return true;
            }

            if (closePromise.isClosed())
            {
                return false;
            }
            boolean hasNext = srcIterator.hasNext();
            //
            // continue to read as we filter 'in' the next object
            while (hasNext)
            {
                this.nextObject = nextImpl();
                this.nextObjectAccessed = false;

                if (filterPredicate.test(this.nextObject))
                {
                    break;
                }
                hasNext = srcIterator.hasNext();
            }
            //
            // and then if its acceptable decide whether we have taken enough
            if (hasNext && !takeWhilePredicate.test(this.nextObject))
            {
                hasNext = false;
            }
            //
            // finally if we have no next then close
            if (!hasNext)
            {
                this.nextObject = null;
                close();
            }
            return hasNext;
        }

        private S nextImpl()
        {
            S next = srcIterator.next();
            returnedSoFar++;
            return next;
        }

        @Override
        public T next()
        {
            if (!hasNext())
            {
                throw new NoSuchElementException();
            }
            nextObjectAccessed = true;
            return mapper.apply(this.nextObject);
        }

        @Override
        public void close()
        {
            closePromise.close();
        }

        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}
