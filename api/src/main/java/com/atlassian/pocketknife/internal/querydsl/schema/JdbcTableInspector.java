package com.atlassian.pocketknife.internal.querydsl.schema;

import io.atlassian.fugue.Option;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Optional;

import static io.atlassian.fugue.Option.none;

/**
 * This can inspect tables via JDBC
 */
@Component
public class JdbcTableInspector {

    private static final String SCHEMA_NAME_KEY = "TABLE_SCHEM";
    private static final String TABLE_NAME_KEY = "TABLE_NAME";
    private static final String COLUMN_NAME_KEY = "COLUMN_NAME";

    /**
     * This will inspect the database for a logical table name given a schema and logical table name
     *
     * @param connection       the JDBC connection
     * @param schema           the schema to match
     * @param logicalTableName the logical table name
     *
     * @return the info about the table
     */
    public JdbcTableAndColumns inspectTableAndColumns(Connection connection, Optional<String> schema, String logicalTableName) {

        // we have to get all tables because the case of the table is unknown and if we
        // give filter by table name, we will get it wrong
        String lookupSchemaName;
        try {
            lookupSchemaName = getSchemaPattern(connection.getMetaData(), schema);
        } catch (SQLException sqlEx) {
            throw new RuntimeException("Unable to enquire connection metadata the system", sqlEx);
        }
        try (ResultSet resultSet = connection.getMetaData().getTables(null, lookupSchemaName, null, null)) {
            while (resultSet.next()) {
                final String realSchemaName = resultSet.getString(SCHEMA_NAME_KEY);
                final String realTableName = resultSet.getString(TABLE_NAME_KEY);
                //double-check: says we query with lookupSchemaName= null, we expect the realSchemaName that returned is null, too.
                boolean matches = matchesTableAndSchema(lookupSchemaName, realSchemaName, logicalTableName, realTableName);
                if (matches) {
                    // get our columns
                    LinkedHashSet<String> tableColumns = inspectColumnNames(connection, realTableName);
                    // we have our table - we can exit this result set - try with resources for the win!
                    return new JdbcTableAndColumns(Option.some(realTableName), tableColumns);
                }
            }
        } catch (SQLException sqlEx) {
            throw new RuntimeException("Unable to enquire table names available in the system", sqlEx);
        }
        return new JdbcTableAndColumns(none(), new LinkedHashSet<>());
    }

    private LinkedHashSet<String> inspectColumnNames(final Connection connection, String realTableName) throws SQLException {
        LinkedHashSet<String> columnNames = new LinkedHashSet<>();
        final DatabaseMetaData metaData = connection.getMetaData();
        try (ResultSet columnResultSet = metaData.getColumns(null, null, realTableName, null)) {
            while (columnResultSet.next()) {
                //
                // so this should be redundant since we specified the table in the query
                // but who knows in JDBC land so lets protect ourselves
                // and all laugh about this later over beers
                //
                final String tableName = columnResultSet.getString(TABLE_NAME_KEY);
                boolean matches = matchesTableName(tableName, realTableName);
                if (matches) {
                    final String columnName = columnResultSet.getString(COLUMN_NAME_KEY);
                    columnNames.add(columnName);
                }
            }
        }
        return columnNames;
    }

    private boolean matchesTableAndSchema(String lookupSchemaName, String realSchemaName, String logicalTableName, String realTableName) {
        return matchesTableName(logicalTableName, realTableName) && matchesSchema(lookupSchemaName, realSchemaName);
    }

    private boolean matchesTableName(String logicalTableName, String realTableName) {
        return logicalTableName.equalsIgnoreCase(realTableName);
    }

    private boolean matchesSchema(String lookupSchemaName, String realSchemaName) {
        if (lookupSchemaName != null) {
            return lookupSchemaName.equalsIgnoreCase(realSchemaName);
        } else {
            return realSchemaName == null;
        }
    }

    /**
     * Lookup schema name according to connection's DatabaseMetaData
     * Note: this code is copied from EntityEngine's org.ofbiz.core.entity.jdbc.DatabaseUtil class
     * https://bitbucket.org/atlassian/entity-engine/raw/master/entity/src/main/java/org/ofbiz/core/entity/jdbc/DatabaseUtil.java
     * If entity engine uses schema name, we should too and vice versa
     */
    private static String getSchemaPattern(final DatabaseMetaData dbData, Optional<String> schemaName) throws SQLException
    {

        if (dbData.supportsSchemasInTableDefinitions())
        {
            if (schemaName.isPresent() && schemaName.get().length() > 0)
            {
                return schemaName.get();
            }
            else if ("Oracle".equalsIgnoreCase(dbData.getDatabaseProductName()))
            {
                // For Oracle, the username is the schema name
                return dbData.getUserName();
            }
        }
        return null;
    }
}
