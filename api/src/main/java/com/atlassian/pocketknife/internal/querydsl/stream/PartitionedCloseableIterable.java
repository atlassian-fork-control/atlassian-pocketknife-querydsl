package com.atlassian.pocketknife.internal.querydsl.stream;

import com.atlassian.annotations.Internal;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.types.Expression;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * Generic implementation of {@link CloseableIterable} that joins multiple {@link CloseableIterable} into one.
 * <p>
 * It allows the actions to be performed as though they are a single iterable, when in reality they are a ordered
 * collection of multiple iterables.
 *
 * @param <T> the type of object in the iterable
 */
@Internal
public class PartitionedCloseableIterable<T> implements CloseableIterable<T> {

    private final List<CloseableIterable<T>> iterables;

    /**
     * The list of iterables
     *
     * @param iterables Iterables, where null will be handled as though an emptyList
     */
    public PartitionedCloseableIterable(List<CloseableIterable<T>> iterables) {
        this.iterables = iterables == null ? emptyList() : ImmutableList.copyOf(iterables);
    }

    @Override
    public CloseableIterator<T> iterator() {
        return new PartitionedCloseableIterator<>(iterables.stream().map(CloseableIterable::iterator).collect(toList()));
    }

    @Override
    public Optional<T> fetchFirst() {
        try {
            return iterables.stream().findFirst().flatMap(CloseableIterable::fetchFirst);
        } finally {
            close();
        }
    }

    @Override
    public CloseableIterable<T> take(int n) {
        final List<CloseableIterable<T>> newIterables = newArrayList();

        final AtomicInteger count = new AtomicInteger(0);

        for (CloseableIterable<T> iterable : iterables) {
            newIterables.add(iterable.takeWhile(t -> count.getAndIncrement() < n));
        }

        return new PartitionedCloseableIterable<>(newIterables);
    }

    @Override
    public CloseableIterable<T> takeWhile(Predicate<T> takeWhilePredicate) {
        final List<CloseableIterable<T>> newIterables = newArrayList();

        final AtomicBoolean ok = new AtomicBoolean(true);

        for (CloseableIterable<T> iterable : iterables) {
            newIterables.add(iterable.takeWhile(t -> {
                if (ok.get()) {
                    ok.set(takeWhilePredicate.test(t));
                }
                return ok.get();
            }));
        }

        return new PartitionedCloseableIterable<>(newIterables);
    }

    @Override
    public CloseableIterable<T> filter(Predicate<T> filterPredicate) {
        final List<CloseableIterable<T>> newIterables = newArrayList();

        for (CloseableIterable<T> iterable : iterables) {
            newIterables.add(iterable.filter(filterPredicate));
        }

        return new PartitionedCloseableIterable<>(newIterables);
    }

    @Override
    public <D> CloseableIterable<D> map(Function<T, D> mapper) {
        final List<CloseableIterable<D>> newIterables = newArrayList();

        for (CloseableIterable<T> iterable : iterables) {
            newIterables.add(iterable.map(mapper));
        }

        return new PartitionedCloseableIterable<>(newIterables);
    }

    @Override
    public <D> CloseableIterable<D> map(Expression<D> expr) {
        final List<CloseableIterable<D>> newIterables = newArrayList();

        for (CloseableIterable<T> iterable : iterables) {
            newIterables.add(iterable.map(expr));
        }

        return new PartitionedCloseableIterable<>(newIterables);
    }

    @Override
    public <D> D foldLeft(D initial, BiFunction<D, T, D> combiningFunction) {
        D value = initial;
        for (CloseableIterable<T> iterable : iterables) {
            value = iterable.foldLeft(value, combiningFunction);
        }
        return value;
    }

    @Override
    public void foreach(Consumer<T> effect) {
        iterables.forEach(i -> i.foreach(effect));
    }

    @Override
    public void close() {
        iterables.forEach(CloseableIterable::close);
    }

    /**
     * A partitioned {@link CloseableIterator} implementation that works on a collection of {@link CloseableIterator}
     *
     * @param <T> the type of object in the iterator
     */
    @VisibleForTesting
    static class PartitionedCloseableIterator<T> implements CloseableIterator<T> {

        private final List<CloseableIterator<T>> iterators;

        /**
         * The list of iterators
         *
         * @param iterators Iterables, where null will be handled as though an emptyList
         */
        PartitionedCloseableIterator(List<CloseableIterator<T>> iterators) {
            this.iterators = iterators == null ? emptyList() : ImmutableList.copyOf(iterators);
        }

        @Override
        public void close() {
            iterators.forEach(CloseableIterator::close);
        }

        @Override
        public boolean hasNext() {
            return iterators.stream().anyMatch(Iterator::hasNext);
        }

        @Override
        public T next() {
            final Optional<CloseableIterator<T>> first = iterators.stream().filter(Iterator::hasNext).findFirst();
            if (first.isPresent()) {
                return first.get().next();
            } else {
                throw new NoSuchElementException();
            }
        }
    }

}
