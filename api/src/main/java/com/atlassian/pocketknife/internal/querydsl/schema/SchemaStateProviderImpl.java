package com.atlassian.pocketknife.internal.querydsl.schema;

import io.atlassian.fugue.Option;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaState;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaStateProvider;
import com.querydsl.core.types.Path;
import com.querydsl.sql.RelationalPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;
import static com.atlassian.pocketknife.api.querydsl.schema.SchemaState.Presence.DIFFERENT;
import static com.atlassian.pocketknife.api.querydsl.schema.SchemaState.Presence.MISSING;
import static com.atlassian.pocketknife.api.querydsl.schema.SchemaState.Presence.SAME;

@Component
@ExportAsService
@ParametersAreNonnullByDefault
public class SchemaStateProviderImpl implements SchemaStateProvider {

    private final JdbcTableInspector tableInspector;
    private final ProductSchemaProvider productSchemaProvider;

    @Autowired
    public SchemaStateProviderImpl(ProductSchemaProvider productSchemaProvider, JdbcTableInspector tableInspector) {
        this.tableInspector = tableInspector;
        this.productSchemaProvider = productSchemaProvider;
    }


    @Override
    public SchemaState getSchemaState(Connection connection, RelationalPath<?> relationalPath) {
        String logicalTableName = relationalPath.getTableName();
        JdbcTableAndColumns tableAndColumns = tableInspector.inspectTableAndColumns(connection, productSchemaProvider.getProductSchema(), logicalTableName);

        final Map<Path, SchemaState.Presence> columnState = new HashMap<>();
        final List<Path<?>> relationalColumns = relationalPath.getColumns();

        Set<String> addedColumns = new LinkedHashSet<>();
        SchemaState.Presence tablePresence;
        Option<String> tableName = tableAndColumns.getTableName();
        if (tableName.isDefined()) {
            // we start as the same until we work out we are in fact different
            tablePresence = SAME;

            LinkedHashSet<String> physicalTableColumns = tableAndColumns.getColumnNames();
            for (Path<?> col : relationalColumns) {
                String logicalColumnName = col.getMetadata().getName();
                Option<String> columnName = findPhysicalColumn(logicalColumnName, physicalTableColumns);

                // we only do 'missing' or 'same' for columns at present based on name, we
                // can do 'different' later based on definition say
                SchemaState.Presence columnPresence;
                if (columnName.isDefined()) {
                    columnPresence = SAME;
                } else {
                    tablePresence = DIFFERENT;
                    columnPresence = MISSING;

                }
                columnState.put(col, columnPresence);
            }
            // if we have more physical columns than logical we are still different
            if (relationalColumns.size() != physicalTableColumns.size()) {
                tablePresence = DIFFERENT;
            }
            // whats been added versus the logical definition
            for (String physicalColumn : physicalTableColumns) {
                boolean foundInLogical = hasLogicalColumn(relationalColumns, physicalColumn);
                if (!foundInLogical) {
                    addedColumns.add(physicalColumn);
                }
            }
        } else {
            tablePresence = MISSING;
            // if the table is missing, all the columns are missing
            relationalColumns.forEach(col -> columnState.put(col, MISSING));
        }

        return new SchemaStateImpl(relationalPath, tablePresence, columnState, addedColumns);
    }

    private Option<String> findPhysicalColumn(String logicalColumnName, LinkedHashSet<String> tableColumns) {
        for (String physicalColumName : tableColumns) {
            if (logicalColumnName.equalsIgnoreCase(physicalColumName)) {
                return some(physicalColumName);
            }
        }
        return none();
    }

    private boolean hasLogicalColumn(List<Path<?>> relationalColumns, String physicalColumn) {
        return relationalColumns.stream()
                .anyMatch(logicalColumn ->
                        logicalColumn.getMetadata().getName().equalsIgnoreCase(physicalColumn));
    }
}
