package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.internal.querydsl.listener.AbstractDetailedLoggingListener;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLListenerContext;

import java.util.Set;

/**
 * This listener allows us to listen to the stages of query creation and gives us a chance
 * to lazily work out what tables are being used and hence what their real "case" name is.
 */
public class SchemaOverrideListener extends AbstractDetailedLoggingListener {

    private final Configuration configuration;
    private final SchemaOverrider schemaOverrider;

    public SchemaOverrideListener(Configuration configuration, SchemaOverrider schemaOverrider) {
        this.configuration = configuration;
        this.schemaOverrider = schemaOverrider;
    }

    private void visit(SQLListenerContext context) {
        Set<RelationalPath<?>> relationalPaths = new RelationPathsInQueryMetadata().capture(context.getMetadata());
        schemaOverrider.registerOverrides(context.getConnection(), configuration, relationalPaths);
    }

    @Override
    public void start(SQLListenerContext context) {
        visit(context);
    }

}
