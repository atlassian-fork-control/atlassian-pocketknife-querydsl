package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.OptionalAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;

@Component
public class OptionalAwareDatabaseAccessorImpl implements OptionalAwareDatabaseAccessor {
    private static final Logger log = LoggerFactory.getLogger(OptionalAwareDatabaseAccessorImpl.class);

    private final DatabaseAccessor databaseAccessor;

    @Autowired
    public OptionalAwareDatabaseAccessorImpl(DatabaseAccessor databaseAccessor) {
        this.databaseAccessor = databaseAccessor;
    }

    @Override
    public <T> Optional<T> runInNewOptionalAwareTransaction(final Function<DatabaseConnection, Optional<T>> function, final OnRollback onRollback) {
        try {
            return databaseAccessor.runInNewTransaction(databaseConnection -> {
                final Optional<T> optional = requireNonNull(function.apply(databaseConnection), "Callback result must not be null");
                if (!optional.isPresent()) {
                    // if empty is returned, throw our runtime exception to trigger a rollback, but catch and return empty
                    throw new OptionalAwareDatabaseAccessorTriggerRollbackException();
                }
                return optional;
            }, onRollback);
        } catch (OptionalAwareDatabaseAccessorTriggerRollbackException e) {
            if (log.isDebugEnabled()) {
                log.debug("Rollback was requested due to empty being returned from optional aware transaction");
            }
            return empty();
        }
    }

    @Override
    public <T> Optional<T> runInOptionalAwareTransaction(final Function<DatabaseConnection, Optional<T>> function, final OnRollback onRollback) {
        try {
            return databaseAccessor.runInTransaction(databaseConnection -> {
                final Optional<T> optional = requireNonNull(function.apply(databaseConnection), "Callback result must not be null");
                if (!optional.isPresent()) {
                    // if empty is returned, throw our runtime exception to trigger a rollback, but catch and return empty
                    throw new OptionalAwareDatabaseAccessorTriggerRollbackException();
                }
                return optional;
            }, onRollback);
        } catch (OptionalAwareDatabaseAccessorTriggerRollbackException e) {
            if (log.isDebugEnabled()) {
                log.debug("Rollback was requested due to empty being returned from optional aware transaction");
            }
            return empty();
        }
    }

    private static class OptionalAwareDatabaseAccessorTriggerRollbackException extends RuntimeException {
        OptionalAwareDatabaseAccessorTriggerRollbackException() {
            super("RuntimeException to trigger a transaction rollback");
        }
    }
}
