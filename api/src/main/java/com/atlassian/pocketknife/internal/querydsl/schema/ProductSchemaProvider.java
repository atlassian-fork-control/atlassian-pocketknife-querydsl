package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * A simple component to wrap how we get the product schema.  This helps simplify our dependencies
 */
@Component
public class ProductSchemaProvider {

    private final TransactionalExecutorFactory executorFactory;

    @Autowired
    public ProductSchemaProvider(@ComponentImport TransactionalExecutorFactory executorFactory) {
        this.executorFactory = executorFactory;
    }

    public Optional<String> getProductSchema() {
        return Optional.ofNullable(executorFactory.createReadOnly().getSchemaName().getOrNull());
    }


}
