package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.dml.SQLDeleteClause;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.sql.Savepoint;

/**
 * Calls to {@link DatabaseAccessor} give you a DatabaseConnection you can use to make SQL queries and return results.
 * <p>
 * You can use the {@link #select(com.querydsl.core.types.Expression[])} method to easily create SELECT queries
 * <pre>
 *     databaseConnection.select(EMPLOYEE.ID, EMPLOYEE.NAME).from(EMPLOYEE).where(EMPLOYEE.NAME.contains("Smith")).orderBy(EMPLOYEE.NAME.asc()).fetch();
 * </pre>
 * <p>
 * DML statements can by created via as well
 * <pre>
 *
 *     databaseConnection.insert(SURVEY).columns(SURVEY.ID, SURVEY.NAME).values(3, "Hello").execute();
 *     databaseConnection.update(SURVEY).where(SURVEY.NAME.eq("Smith")).set(SURVEY.NAME, "S").execute();
 *     databaseConnection.delete(SURVEY).where(SURVEY.NAME.eq("Smith")).execute();
 * </pre>
 *
 * @see <a href="http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html">More information on
 * QueryDSL statements</a>
 * @since 2.0.0
 */
@ParametersAreNonnullByDefault
public interface DatabaseConnection extends AutoCloseable {
    /**
     * Create a new {@link SQLQuery} instance with the given single projection starting at {@link SQLQuery#select(Expression)}}
     *
     * @param expression the projection
     * @return a select query of the expression type
     * @see <a href="http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html">More information on
     * QueryDSL statements</a>
     */
    <T> SQLQuery<T> select(Expression<T> expression);

    /**
     * Create a new {@link SQLQuery} instance with the given columns projection starting at {@link SQLQuery#select(Expression[])}
     *
     * @param expressions the columns projection
     * @return an query that will return {@link com.querydsl.core.Tuple}s
     * @see <a href="http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html">More information on
     * QueryDSL statements</a>
     */
    SQLQuery<Tuple> select(Expression<?>... expressions);

    /**
     * Create a new {@link SQLQuery} instance with the given single projection starting at {@link SQLQuery#from(Expression)}
     *
     * @param path the relational path projection
     * @return a select query of the expression type
     * @see <a href="http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html">More information on
     * QueryDSL statements</a>
     */
    <T> SQLQuery<T> from(RelationalPath<T> path);


    /**
     * Create a new SQLInsertClause to insert data into the database
     *
     * @param entity the table entity to insert data into
     * @return an insert clause
     */
    SQLInsertClause insert(RelationalPath<?> entity);

    /**
     * Create a new SQLDeleteClause to delete data from the database
     *
     * @param entity the table entity to delete data from
     * @return a delete clause
     */
    SQLDeleteClause delete(RelationalPath<?> entity);

    /**
     * Create a new SQLUpdateClause to update data in the database
     *
     * @param entity the table entity to update data on
     * @return an update clause
     */
    SQLUpdateClause update(RelationalPath<?> entity);

    /**
     * @return A {@link com.querydsl.sql.SQLQueryFactory} that can be used to create and execute SQL
     * @see <a href="http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html">More information on
     * QueryDSL statements</a>
     */
    SQLQueryFactory query();


    /**
     * @return the underlying JDBC connection wrapped by this object.
     */
    Connection getJdbcConnection();

    /**
     * @return true if the connection is in auto commit mode
     */
    boolean isAutoCommit();

    /**
     * @return true if the connection is in fact closed
     */
    boolean isClosed();

    /**
     * @return true if this was run inside {@link DatabaseAccessor#runInTransaction(java.util.function.Function)}
     */
    boolean isInTransaction();

    /**
     * @return true if the connection is an externally managed connection
     */
    boolean isExternallyManaged();

    /**
     * Convenience method to set autocommit mode on the database connection without the annoying SQLException.
     * <p>
     * It is invalid to call this method if you are running via inside {@link DatabaseAccessor#runInTransaction(java.util.function.Function)}
     * because the connection semantics are set for you.
     *
     * @param autoCommit <code>true</code> to enable auto-commit mode, <code>false</code> to disable it
     * @throws java.lang.RuntimeException    wrapping any underlying SQLException.
     * @throws UnsupportedOperationException if this connection is an externally managed connection
     */
    void setAutoCommit(boolean autoCommit);

    /**
     * Commits all changes made since the previous commit or rollback. This method should be used only when auto-commit
     * mode has been disabled.
     * <p>
     * It is invalid to call this method if you are running via inside {@link DatabaseAccessor#runInTransaction(java.util.function.Function)}
     * because the connection semantics are set for you.
     *
     * @throws java.lang.RuntimeException    wrapping any underlying SQLException.
     * @throws UnsupportedOperationException if this connection is an externally managed connection
     * @see #setAutoCommit
     */
    void commit();

    /**
     * Undoes all changes made in the current transaction. This method should be used only when auto-commit mode has
     * been disabled. You normally don't need to call this explicitly - instead throwing a RuntimeException will
     * indicate that you want to rollback.
     * <p>
     * It is invalid to call this method if you are running via inside {@link DatabaseAccessor#runInTransaction(java.util.function.Function)}
     * because the connection semantics are set for you.
     *
     * @throws java.lang.RuntimeException    wrapping any underlying SQLException.
     * @throws UnsupportedOperationException if this connection is an externally managed connection
     * @see #setAutoCommit
     */
    void rollback();

    /**
     * This will rollback to a specified save point,
     * and is available no matter what type of underlying connection you have.
     *
     * @param savepoint the place to rollback to
     * @see Connection#rollback(Savepoint)
     */
    void rollback(Savepoint savepoint);

    /**
     * This will release the specified save point,
     * and is available no matter what type of underlying connection you have.
     *
     * @param savepoint the save point ot release
     * @see Connection#releaseSavepoint(Savepoint)
     */
    void releaseSavepoint(Savepoint savepoint);

    /**
     * This will set a unnamed save point,
     * and is available no matter what type of underlying connection you have.
     *
     * @return the save point
     * @see Connection#setSavepoint()
     */
    Savepoint setSavepoint();

    /**
     * This will set a named save point,
     * and is available no matter what type of underlying connection you have.
     *
     * @param name the save point name
     * @return the save point
     * @see Connection#setSavepoint(String)
     */
    Savepoint setSavepoint(String name);

    /**
     * @return the dialect configuration for this database connection
     */
    DialectProvider.Config getDialectConfig();

}
