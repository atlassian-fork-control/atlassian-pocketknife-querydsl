package com.atlassian.pocketknife.api.querydsl.schema;

import com.atlassian.annotations.PublicApi;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.querydsl.sql.RelationalPath;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;

/**
 * This can provide information on the physical state of the schema at runtime to allow
 * code to perform 'fast-five' code techniques when tables or table columns are missing
 * or changed.
 *
 * @since v4.0
 */
@PublicApi
@ParametersAreNonnullByDefault
public interface SchemaStateProvider {
    /**
     * This will return the state of the schema with respect to the given {@link RelationalPath} which
     * you can use to try and detect 'fast-five' changes about the presence of tables and their columns
     *
     * @param connection     the JDBC connection (see {@link DatabaseConnection#getJdbcConnection() for a connection}
     * @param relationalPath the relational path (aka Table) in play
     *
     * @return the schema state for that relational path
     */
    SchemaState getSchemaState(Connection connection, RelationalPath<?> relationalPath);

}
