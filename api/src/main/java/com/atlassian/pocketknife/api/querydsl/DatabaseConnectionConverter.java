package com.atlassian.pocketknife.api.querydsl;

import java.sql.Connection;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * This can be used to turn JDBC connections into {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection}
 * objects ready for use.  This is useful if you have a JDBC connection from some external source, like SAL, and you
 * want to execute PK QDSL code on it.
 *
 * @since v2.0.0
 */
@ParametersAreNonnullByDefault
public interface DatabaseConnectionConverter
{
    /**
     * Makes a JDBC Connection into a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection} as an un-managed
     * connection
     *
     * @param jdbcConnection the connection to convert
     * @return a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection}
     */
    DatabaseConnection convert(Connection jdbcConnection);

    /**
     * Makes a JDBC Connection into a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection} where the commit semantics (autocommit etc..) are
     * externally managed by the supplier of the original connection.
     * <p>
     * Because the connection and its transaction are externally managed, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     *
     * @param jdbcConnection the connection to convert
     * externally
     * @return a {@link com.atlassian.pocketknife.api.querydsl.DatabaseConnection}
     */
    DatabaseConnection convertExternallyManaged(Connection jdbcConnection);
}
