package com.atlassian.pocketknife.api.querydsl;

import com.atlassian.pocketknife.api.querydsl.util.OnRollback;

import java.util.Optional;
import java.util.function.Function;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;

/**
 * This gives you the ability to access the database, and unlike {@link DatabaseAccessor} that will only rollback on
 * {@link RuntimeException}, this will allow to use a Functional approach and provide rollback option on
 * {@link Optional#empty()}
 *
 * @since v4.3
 */
public interface OptionalAwareDatabaseAccessor {

    /**
     * Gives you controlled access to a {@link DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will ALWAYS run the callback in a NEW transaction.  This is different to {@link #runInOptionalAwareTransaction(Function)}
     * <p>
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully with an {@link Optional#isPresent()}, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then either throw a RuntimeException or return {@link Optional#empty()}. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInNewTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException or returns Optional.empty() to indicate rollback required
     *             return doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback the callback function that can run one or more queries with the managed connection. The returned value
     *                 can not be null. Consider {@link io.atlassian.fugue.Unit} when return value would otherwise have no meaning.
     * @param <T>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @deprecated Use {@link #runInNewOptionalAwareTransaction(Function, OnRollback)} instead. Since v4.4.0.
     */
    @Deprecated
    default <T> Optional<T> runInNewOptionalAwareTransaction(Function<DatabaseConnection, Optional<T>> callback) {
        return runInNewOptionalAwareTransaction(callback, NOOP);
    }

    /**
     * Gives you controlled access to a {@link DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will ALWAYS run the callback in a NEW transaction.  This is different to {@link #runInOptionalAwareTransaction(Function, OnRollback)}
     * <p>
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully with an {@link Optional#isPresent()}, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then either throw a RuntimeException or return {@link Optional#empty()}. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInNewTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException or returns Optional.empty() to indicate rollback required
     *             return doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback   the callback function that can run one or more queries with the managed connection. The returned value
     *                   can not be null. Consider {@link io.atlassian.fugue.Unit} when return value would otherwise have no meaning.
     * @param onRollback A callback that will be executed in the event that transaction is rolled back
     * @param <T>        the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 4.4.0
     */
    <T> Optional<T> runInNewOptionalAwareTransaction(Function<DatabaseConnection, Optional<T>> callback, OnRollback onRollback);

    /**
     * Gives you controlled access to a {@link DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will attempt to run the callback within an existing transaction if one is running within this thread.
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully with an {@link Optional#isPresent()}, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then either throw a RuntimeException or return {@link Optional#empty()}. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * This method is different to {@link #runInNewOptionalAwareTransaction(Function)}
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException or returns Optional.empty() to indicate rollback required
     *             return doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback the callback function that can run one or more queries with the managed connection. The returned value
     *                 can not be null. Consider {@link io.atlassian.fugue.Unit} when return value would otherwise have no meaning.
     * @param <T>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @deprecated Use {@link #runInOptionalAwareTransaction(Function, OnRollback)} instead. Since v4.4.0.
     */
    @Deprecated
    default <T> Optional<T> runInOptionalAwareTransaction(Function<DatabaseConnection, Optional<T>> callback) {
        return runInOptionalAwareTransaction(callback, NOOP);
    }

    /**
     * Gives you controlled access to a {@link DatabaseConnection} that allows
     * you to execute SQL statements as defined in the callback with managed transaction semantics.
     * <p>
     * This method will attempt to run the callback within an existing transaction if one is running within this thread.
     * For that reason, the commit and rollback will be completely managed for you. If the callback returns
     * successfully with an {@link Optional#isPresent()}, then this indicates that the underlying transaction is allowed to be committed at the appropriate
     * time. If you want to rollback the transaction then either throw a RuntimeException or return {@link Optional#empty()}. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * <p>
     * This method is different to {@link #runInNewOptionalAwareTransaction(Function, OnRollback)}
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you, then
     * specific {@link DatabaseConnection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link DatabaseConnection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link DatabaseConnection#commit()} commit will occur in the broader transaction context if no errors are encountered </li>
     * <li>{@link DatabaseConnection#rollback()} rollback will occur if a RuntimeException is thrown from the callback function </li>
     * <li>{@link DatabaseConnection#close()} the connection will be returned to the pool for you at the appropriate time</li>
     * </ul>
     * Example Usage:
     * <pre>
     *     databaseAccessor.runInTransaction(
     *         connection -&gt; {
     *             // throws RuntimeException or returns Optional.empty() to indicate rollback required
     *             return doSomeUpdates(connection);
     *         }
     *     );
     * </pre>
     *
     * @param callback   the callback function that can run one or more queries with the managed connection. The returned value
     *                   can not be null. Consider {@link io.atlassian.fugue.Unit} when return value would otherwise have no meaning.
     * @param onRollback A callback that will be executed in the event that transaction is rolled back
     * @param <T>        the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 4.4.0
     */
    <T> Optional<T> runInOptionalAwareTransaction(Function<DatabaseConnection, Optional<T>> callback, OnRollback onRollback);

    /**
     * A synonym for the more semantically named {@link #runInOptionalAwareTransaction(Function)} method
     *
     * @param callback the callback function that can run one or more queries with the managed connection.
     * @param <T>      the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @deprecated Use {@link #runOptionalAware(Function, OnRollback)} instead. Since v4.4.0.
     */
    @Deprecated
    default <T> Optional<T> runOptionalAware(Function<DatabaseConnection, Optional<T>> callback) {
        return runInOptionalAwareTransaction(callback);
    }

    /**
     * A synonym for the more semantically named {@link #runInOptionalAwareTransaction(Function, OnRollback)} method
     *
     * @param callback   the callback function that can run one or more queries with the managed connection.
     * @param onRollback A callback that will be executed in the event that transaction is rolled back
     * @param <T>        the return type of the callback and of this method
     * @return the value as returned by the callback function
     * @since 4.4.0
     */
    default <T> Optional<T> runOptionalAware(Function<DatabaseConnection, Optional<T>> callback, OnRollback onRollback) {
        return runInOptionalAwareTransaction(callback, onRollback);
    }

}
