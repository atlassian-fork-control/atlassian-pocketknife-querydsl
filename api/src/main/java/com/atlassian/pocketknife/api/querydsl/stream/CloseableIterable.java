package com.atlassian.pocketknife.api.querydsl.stream;

import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.types.Expression;

import java.io.Closeable;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * A closeable iterable can be closed.  This is uber important in the SQL world because {@link java.sql.ResultSet}s  MUST be
 * closed when the results have been consumed.
 * <p>
 * They must also be closed in try finally blocks as well for defensive reasons such as your code throwing exceptions
 * during its processing.
 */
public interface CloseableIterable<T> extends Iterable<T>, Closeable {
    /**
     * @return the closeable iterator used under the covers
     */
    CloseableIterator<T> iterator();

    /**
     * This will fetch the first result in the iterable and then call {@link #close()}, ensuring that after this
     * method is called, the underlying iterator is always closed.
     * <p>
     * <em>NOTE</em> - You really should try to limit to a single result in the SQL query layer via a limit(n) statement on the query say.
     * Relational databases will always do a better job of this if its possible.  This method
     * should only be used when its NOT possible to limit in SQL.
     *
     * @return the first result or an option of none if there is no results
     */
    Optional<T> fetchFirst();

    /**
     * Returns the n first elements of this Iterable as another Iterable, or else the whole Iterable, if it has less
     * than n elements.
     * <p>
     * <em>NOTE</em> - once the iterable reaches the limit n it will be automatically closed.
     * <p>
     * <em>NOTE</em> - You really should push most limiting into the SQL query layer via a limit(n) statement on the query.
     * Relational databases  will always do a better job of this if its possible.  This method
     * should only be used when its NOT possible to limit in SQL.
     *
     * @param n the number of elements to return - this must be &gt;= to 0
     * @return a limited Iterable
     * @throws java.lang.IllegalStateException if n is &lt; 0
     */
    CloseableIterable<T> take(int n);

    /**
     * Returns the elements until such time as the predicate returns false or the underlying elements run out.
     * <p>
     * <em>NOTE</em> - once the Predicate returns false the Iterable it will be automatically closed
     * <p>
     * <em>NOTE</em> - You really should push most limiting into the SQL query layer via a limit(n) statement on the query.
     * Relational databases  will always do a better job of this if its possible.  This method
     * should only be used when its NOT possible to limit in SQL
     *
     * @param takeWhilePredicate the predicate to evaluate against each element
     * @return a limited Iterable
     */
    CloseableIterable<T> takeWhile(Predicate<T> takeWhilePredicate);

    /**
     * This will filter out objects that don't match the specified predicate.
     * <p>
     * <em>NOTE</em> : You really should push most filtering into the SQL query layer.  Relational databases will always do a
     * better job of this if its possible.  This filter method should only be used when its NOT possible to filter in
     * SQL
     * </p>
     *
     * @param filterPredicate the predicate to apply
     * @return an iterable of domain objects that match the predicate
     */
    CloseableIterable<T> filter(Predicate<T> filterPredicate);

    /**
     * This will map a fetch query of &lt;T&gt; objects into an Iterable of domain object D
     *
     * @param mapper the tuple mapper function
     * @param <D>    the domain type
     * @return an iterable of domain objects
     */
    <D> CloseableIterable<D> map(Function<T, D> mapper);

    /**
     * This will map a fetch query of Tuple objects into an Iterable of domain object T using the expression contained
     * within each Tuple
     *
     * @param expr the SimpleExpression contained within the Tuple
     * @param <D>  the domain type
     * @return an iterable of domain objects
     */
    <D> CloseableIterable<D> map(Expression<D> expr);


    /**
     * Fold the combiningFunction over the query and return the accumulated value, executes immediately and closes the
     * iterable.
     *
     * @param initial           The initial value to be passed to #combiningFunction
     * @param combiningFunction A function that takes a query T value and the accumulating value and returns the new
     *                          accumulating value
     * @return The result of applying combiningFunction to each element in the list starting with #initial and passing
     * in the result of the previous call as it traverses the result.
     */
    <D> D foldLeft(final D initial, BiFunction<D, T, D> combiningFunction);


    /**
     * Calls the effect for each result in the CloseableIterable
     *
     * @param effect the side effect code to be called
     */
    void foreach(final Consumer<T> effect);

    /**
     * Closes this iterator and releases any system resources associated with it. If the iterator is already closed then
     * invoking this method has no effect.
     */
    void close();

}
