package com.atlassian.pocketknife.api.querydsl.tuple;

import com.atlassian.annotations.Internal;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.NumberExpression;

import java.math.BigDecimal;
import java.util.function.Function;

/**
 * Functions to map values from within a Tuple
 */
@Internal
class TupleMapper
{
    <T> Function<Tuple, T> column(final Expression<T> expr)
    {
        return tuple -> {
            T t = tuple.get(expr);
            return t == null ? null : t;
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Integer> toInt(final NumberExpression<T> expr)
    {
        return tuple -> {
            T t = tuple.get(expr);
            return t == null ? null : t.intValue();
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Long> toLong(final NumberExpression<T> expr)
    {
        return tuple -> {
            T t = tuple.get(expr);
            return t == null ? null : t.longValue();
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Float> toFloat(final NumberExpression<T> expr)
    {
        return tuple -> {
            T t = tuple.get(expr);
            return t == null ? null : t.floatValue();
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, Double> toDouble(final NumberExpression<T> expr)
    {
        return tuple -> {
            T t = tuple.get(expr);
            return t == null ? null : t.doubleValue();
        };
    }

    <T extends Number & Comparable<?>> Function<Tuple, BigDecimal> toBigDecimal(final NumberExpression<T> expr)
    {
        return tuple -> {
            T t = tuple.get(expr);
            if (t instanceof BigDecimal)
            {
                return (BigDecimal) t;
            }
            return t == null ? null : new BigDecimal(t.doubleValue());
        };
    }
}
