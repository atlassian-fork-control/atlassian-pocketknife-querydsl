package com.atlassian.pocketknife.api.querydsl.stream;

import com.atlassian.pocketknife.internal.querydsl.stream.CloseableIterableImpl;
import com.atlassian.pocketknife.internal.querydsl.stream.PartitionedCloseableIterable;
import com.atlassian.pocketknife.internal.querydsl.util.fp.Fp;
import com.mysema.commons.lang.CloseableIterator;

import java.util.List;

/**
 * A helper class for {@link CloseableIterables} and their related classes, with the ability to take
 * QueryDSL {@link com.mysema.commons.lang.CloseableIterator}s and turn them into {@link com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable}s
 * which have extra semantics, close promises and functional helper methods.
 */
public class CloseableIterables {
    /**
     * Returns a {@link com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable} from the specified queryDSL {@link CloseableIterator}.
     * <p>
     * <em>NOTE :</em> Now that this is a once only Iterable. Its backed by a database connection and hence
     * it can only be consumed once.
     *
     * @param closeableIterator the source iterator that is closeable
     * @param closePromise      - the promise that should be executed when the iterator is finished
     * @param <T>               the type of object in the iterator
     * @return a CloseableIterable
     */
    public static <T> CloseableIterable<T> iterable(CloseableIterator<T> closeableIterator, ClosePromise closePromise) {
        return new CloseableIterableImpl<>(closeableIterator, Fp.identity(), closePromise);
    }

    /**
     * Returns a {@link CloseableIterable} from the specified queryDSL {@link CloseableIterator}
     * <p>
     * <em>NOTE :</em> Now that this is a once only Iterable.
     *
     * @param closeableIterator the source iterator that is closeable
     * @param <T>               the type of object in the iterator
     * @return a CloseableIterable
     */
    public static <T> CloseableIterable<T> iterable(CloseableIterator<T> closeableIterator) {
        return new CloseableIterableImpl<>(closeableIterator, Fp.identity(), ClosePromise.NOOP());
    }

    /**
     * Returns a new {@link CloseableIterable} from the specified list of {@link CloseableIterable}
     * <p>
     * This will allow multiple iterables to be combined into a single iterable that can be acted upon as one.
     * <p>
     * The implementation came about as a means to support partitioning of long IN clause queries that are size limited
     * in ORACLE databases. But it's usefulness is not limited to only that case.
     *
     * @param closeableIterables the source iterables that should be acted upon in order. null will be treated as though
     *                           an empty list, and must not be any null values inside the list
     * @param <T>                the type of object in the iterable
     * @return a new CloseableIterable that supports the partitioning
     */
    public static <T> CloseableIterable<T> partitioned(List<CloseableIterable<T>> closeableIterables) {
        return new PartitionedCloseableIterable<>(closeableIterables);
    }
}
