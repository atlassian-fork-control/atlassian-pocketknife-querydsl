package com.atlassian.pocketknife.internal.querydsl.testplugin.tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

/**
 */
public class QIssueTable extends EnhancedRelationalPathBase<QIssueTable>
{
    // Columns
    public final NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public final StringPath SUMMARY = createStringCol("SUMMARY").notNull().build();
    public final StringPath REPORTER = createStringCol("REPORTER").notNull().build();
    public final StringPath CREATOR = createStringCol("CREATOR").notNull().build();

    public QIssueTable(final String tableName)
    {
        super(QIssueTable.class, tableName);
    }

}
